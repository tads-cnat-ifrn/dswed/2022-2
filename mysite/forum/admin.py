from django.contrib import admin
from .models import Usuario, Tema, Pergunta, Resposta

admin.site.register(Usuario)
admin.site.register(Tema)
admin.site.register(Pergunta)
admin.site.register(Resposta)