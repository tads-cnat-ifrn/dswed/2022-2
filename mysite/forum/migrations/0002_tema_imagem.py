# Generated by Django 4.0.6 on 2022-12-05 13:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tema',
            name='imagem',
            field=models.ImageField(blank=True, null=True, upload_to='tema'),
        ),
    ]
