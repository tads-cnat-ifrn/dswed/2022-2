from django.urls import path
from . import views

app_name = 'forum'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('tema/<int:pk>/', views.TemaView.as_view(), name='tema'),
    path('tema/buscar/', views.BuscaView.as_view(), name='busca'),
    path(
        'tema/adicionar/',
        views.NovoTemaView.as_view(), name='add_tema'
    ),
    path(
        'tema/<int:pk>/remover/',
        views.RemoveTemaView.as_view(), name='remove_tema'
    ),
    path(
        'tema/<int:pk>/alterar/',
        views.AlteraTemaView.as_view(), name='altera_tema'
    ),
    path(
        'usuario/adicionar/',
        views.NovoUsuarioView.as_view(), name='add_usuario'
    ),
    path(
        'tema/<int:pk>/perguntar/',
        views.NovaPerguntaView.as_view(), name='add_perg'
    ),
    path(
        'pergunta/<int:pk>/responder/',
        views.AdicionaRespostaView.as_view(), name='add_resp'
    ),
    path(
        'resposta/<int:pk>/votar/',
        views.VotarEmRespostaView.as_view(), name='vota_resp'
    ),
]