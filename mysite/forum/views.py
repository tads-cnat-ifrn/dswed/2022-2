from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import login
from django.views import View
from django.contrib import messages
from .models import Tema, TemaForm, Pergunta, Resposta, Usuario
from django.contrib.auth.models import User

class IndexView(View):
    def get(self, request, *args, **kwargs):
        temas = Tema.objects.all()
        contexto = {'temas': temas}
        return render(request, 'forum/index.html', contexto)

class BuscaView(View):
    def post(self, request, *args, **kwargs):
        busca = request.POST.get('busca')
        termo = request.POST.get('termo')
        if busca and termo:
            if busca == 'tema':
                filtro = 'Temas com "%s" no título' % termo
                retorno = Tema.objects.filter(titulo__icontains=termo)
            else:
                filtro = 'Temas com "%s" no texto de perguntas' % termo
                retorno = Tema.objects.filter(
                    pergunta__enunciado__icontains=termo
                ).distinct()
            contexto = {'temas': retorno, 'busca': True, 'filtro': filtro}
            return render(request, 'forum/index.html', contexto)
        else:
            return HttpResponseRedirect(reverse('forum:index'))

class TemaView(View):
    def get(self, request, *args, **kwargs):
        tema = get_object_or_404(Tema, pk=kwargs['pk'])
        usuario_valido = hasattr(request.user, 'usuario')
        contexto = {'tema': tema, 'usuario_valido': usuario_valido}
        return render(request, 'forum/tema.html', contexto)

class NovoUsuarioView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'forum/novo_usuario.html')
    def post(self, request, *args, **kwargs):
        username = request.POST.get('login')
        senha1 = request.POST.get('senha1')
        senha2 = request.POST.get('senha2')
        email = request.POST.get('email')
        apelido = request.POST.get('apelido')
        if username and senha1 and email and apelido and senha1 == senha2:
            user = User.objects.create_user(username, email, senha1)
            agora = timezone.now()
            usuario = Usuario(user=user, apelido=apelido, ultima_postagem=agora)
            usuario.save()
            login(request, user)
            return HttpResponseRedirect(reverse('forum:index'))
        else:
            contexto = {
                'erro': 'Os campos não podem ficar em branco.'
            }
            return render(request, 'forum/novo_usuario.html', contexto)

def testa_usuario_valido(user):
    if hasattr(user, 'usuario'):
        return True
    return False

@method_decorator(user_passes_test(testa_usuario_valido), name='dispatch')
class NovoTemaView(View):
    def get(self, request, *args, **kwargs):
        form = TemaForm()
        return render(request, 'forum/novo_tema.html', {'form': form})
    def post(self, request, *args, **kwargs):
        form = TemaForm(request.POST, request.FILES)
        if form.is_valid():
            usr = request.user.usuario
            tema = form.save(commit=False)
            tema.usuario = usr
            tema.save()
            messages.success(request,'Tema adicionado com sucesso.')
            return HttpResponseRedirect(reverse('forum:index'))
        else:
            return render(request, 'forum/novo_tema.html', {'form': form})

@method_decorator(user_passes_test(testa_usuario_valido), name='dispatch')
class AlteraTemaView(View):
    def get(self, request, *args, **kwargs):
        tema = get_object_or_404(Tema, pk=kwargs['pk'])
        if not request.user == tema.usuario.user:
            return HttpResponseRedirect(reverse('forum:index'))
        form = TemaForm(instance=tema)
        contexto = {'form': form, 'alteracao': True, 'pk': tema.id}
        return render(request, 'forum/novo_tema.html', contexto)
    def post(self, request, *args, **kwargs):
        tema = get_object_or_404(Tema, pk=kwargs['pk'])
        if not request.user == tema.usuario.user:
            return HttpResponseRedirect(reverse('forum:index'))
        form = TemaForm(request.POST, request.FILES, instance=tema)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('forum:index'))
        else:
            contexto = {'form': form, 'alteracao': True, 'pk': tema.id}
            return render(request, 'forum/novo_tema.html', contexto)

@method_decorator(user_passes_test(testa_usuario_valido), name='dispatch')
class RemoveTemaView(View):
    def get(self, request, *args, **kwargs):
        tema = get_object_or_404(Tema, pk=kwargs['pk'])
        tema.delete()
        messages.warning(request, 'Tema %s removido com sucesso'%tema.titulo)
        return HttpResponseRedirect(reverse('forum:index'))

@method_decorator(user_passes_test(testa_usuario_valido), name='dispatch')
class NovaPerguntaView(View):
    def post(self, request, *args, **kwargs):
        tema = get_object_or_404(Tema, pk=kwargs['pk'])
        enunciado = request.POST.get('enunciado')
        if enunciado:
            usr = request.user.usuario
            nova = Pergunta(enunciado=enunciado, tema=tema, usuario=usr)
            nova.save()
            return HttpResponseRedirect(reverse('forum:tema', args=(tema.id,)))
        else:
            contexto = {
                'tema': tema,
                'erro': 'Preencha o enunciado da pergunta corretamente!'
            }
            return render(request, 'forum/tema.html', contexto)

@method_decorator(user_passes_test(testa_usuario_valido), name='dispatch')
class AdicionaRespostaView(View):
    def post(self, request, *args, **kwargs):
        perg = get_object_or_404(Pergunta, pk=kwargs['pk'])
        txt = request.POST['texto']
        if txt:
            usr = request.user.usuario
            resposta = Resposta(
                texto=txt, pergunta=perg, usuario=usr
            )
            resposta.save()
            return HttpResponseRedirect(reverse(
                'forum:tema', args=(perg.tema.id,)
            ))
        else:
            contexto = {
                'tema': perg.tema,
                'erro': 'Digite uma resposta válida!'
            }
            return render(request, 'forum/tema.html', contexto)

class VotarEmRespostaView(View):
    def get(self, request, *args, **kwargs):
        resposta = get_object_or_404(Resposta, pk=kwargs['pk'])
        resposta.curtidas += 1
        resposta.save()
        return HttpResponseRedirect(reverse(
            'forum:tema', args=(resposta.pergunta.tema.id,)
        ))






