from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User

class Usuario(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE, null = True)
    apelido = models.CharField(max_length = 20)
    ultima_postagem = models.DateTimeField()
    def __str__(self):
        return self.apelido
    class Meta:
        ordering = ['apelido']
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'

class Tema(models.Model):
    titulo = models.CharField(max_length = 100)
    descricao = models.CharField(max_length = 200)
    arquivado = models.BooleanField(default = False)
    imagem = models.ImageField(upload_to='tema', null=True, blank=True)
    data = models.DateField(auto_now_add = True)
    usuario = models.ForeignKey(Usuario, on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return self.titulo

class TemaForm(ModelForm):
    class Meta:
        model = Tema
        fields = ['titulo', 'descricao', 'imagem']

class Pergunta(models.Model):
    enunciado = models.CharField(max_length = 100)
    data = models.DateField(auto_now_add = True)
    curtidas = models.IntegerField(default = 0)
    tema = models.ForeignKey(Tema, on_delete = models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.SET_NULL, null=True)
    def get_respostas(self):
        return self.resposta_set.order_by('-curtidas')
    def __str__(self):
        return self.enunciado

class Resposta(models.Model):
    texto = models.CharField(max_length = 100)
    data = models.DateTimeField(auto_now_add = True)
    curtidas = models.IntegerField(default = 0)
    pergunta = models.ForeignKey(Pergunta, on_delete = models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return self.texto

"""
Ilustração de mapeamento de relacionamento muitos-para-muitos

class Usuario(models.Model):
    apelido = models.CharField(max_length = 20)
    ultima_postagem = models.DateTimeField()
    grupos = models.ManyToManyField(Grupo)

class Grupo(models.Model):
    nome = models.CharField(max_length = 30)

"""











