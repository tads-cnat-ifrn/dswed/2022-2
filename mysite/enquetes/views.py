from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from django.views import View
from .models import Pergunta, Alternativa

class IndexView(View):
    def get(self, request, *args, **kwargs):
        lista_enquetes = Pergunta.objects.filter(
            data_pub__lte = timezone.now()
        ).order_by('-data_pub')
        contexto = { 'lista_enquetes': lista_enquetes }
        return render(request, 'enquetes/index.html', contexto)

class DetalhesView(View):
    def get(self, request, *args, **kwargs):
        pergunta = get_object_or_404(Pergunta, pk = kwargs['pk'])
        if pergunta.data_pub >= timezone.now():
            raise Http404('Nenhuma Enquete publicada com essa identificação')
        return render(
            request, 'enquetes/detalhes.html',
            {'pergunta': pergunta}
        )

class ResultadoView(View):
    def get(self, request, *args, **kwargs):
        pergunta = get_object_or_404(Pergunta, pk = kwargs['pk'])
        return render(
            request, 'enquetes/resultado.html',
            {'pergunta': pergunta}
        )

class VotacaoView(View):
    def post(self, request, *args, **kwargs):
        pergunta = get_object_or_404(Pergunta, pk = kwargs['pk'])
        try:
            alt_id = request.POST['alt']
            alternativa = pergunta.alternativa_set.get(pk=alt_id)
        except(KeyError, Alternativa.DoesNotExist):
            return render(request, 'enquetes/detalhes.html', {
                'pergunta': pergunta,
                'error': 'Selecione uma alternativa válida!'
            })
        else:
            alternativa.quant_votos += 1
            alternativa.save()
            return HttpResponseRedirect(reverse(
                'enquetes:resultado', args=(pergunta.id,)
            ))


"""
#### HISTÓRICO DAS VERSÕES ####
###############################

## VIEW INDEX ##
################
----versão 1
    resultado = ',<br/> '.join([p.texto for p in lista_enquetes])
    return HttpResponse(resultado)
----versão 2
    template = loader.get_template('enquetes/index.html')
    return HttpResponse(template.render(contexto, request))
----versão 3
def index(request):
    lista_enquetes = Pergunta.objects.order_by('-data_pub')[:10]
    contexto = { 'lista_enquetes': lista_enquetes }
    return render(request, 'enquetes/index.html', contexto)
----vesão 4
class IndexView(generic.ListView):
    template_name = 'enquetes/index.html'
    context_object_name = 'lista_enquetes'
    def get_queryset(self):
        return Pergunta.objects.order_by('-data_pub')

## VIEW DETALHES ##
###################
----versão 1
    retorno = "<h2>DETALHES da enquete de identificador = %s</h2>"
    return HttpResponse(retorno % enquete_id)
----versão 2
    try:
        pergunta = Pergunta.objects.get(pk = enquete_id)
    except Pergunta.DoesNotExist:
        raise Http404("Identificador de enquete inválido!")
----versão 3
def detalhes(request, enquete_id):
    pergunta = get_object_or_404(Pergunta, pk = enquete_id)
    return render(
        request, 'enquetes/detalhes.html', {'pergunta': pergunta}
    )
----versão 4
class DetalhesView(generic.DetailView):
    model = Pergunta
    template_name = 'enquetes/detalhes.html'
## VIEW RESULTADO ##
####################
----versão 1
def resultado(request, enquete_id):
    pergunta = get_object_or_404(Pergunta, pk = enquete_id)
    return render(
        request, 'enquetes/resultado.html', {'pergunta': pergunta}
    )
----versão 2
class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'enquetes/resultado.html'
## VIEW VOTACAO ##
##################
----versão 1
def votacao(request, enquete_id):
    pergunta = get_object_or_404(Pergunta, pk = enquete_id)
    try:
        alt_id = request.POST['alt']
        alternativa = pergunta.alternativa_set.get(pk=alt_id)
    except(KeyError, Alternativa.DoesNotExist):
        return render(request, 'enquetes/detalhes.html', {
            'pergunta': pergunta,
            'error': 'Selecione uma alternativa válida!'
        })
    else:
        alternativa.quant_votos += 1
        alternativa.save()
        return HttpResponseRedirect(reverse(
            'enquetes:resultado', args=(pergunta.id,)
        ))
"""









