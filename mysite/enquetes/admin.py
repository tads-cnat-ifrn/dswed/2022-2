from django.contrib import admin
from .models import Pergunta, Alternativa

admin.site.site_header = 'Administração do Projeto DSWeb 2022.2'

class AlternativaInline(admin.TabularInline):
    model = Alternativa
    extra = 2

class PerguntaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['texto', 'autor']}),
        ('Informações de data', {'fields': ['data_pub']}),
    ]
    inlines = [AlternativaInline]
    list_display = ('texto', 'id', 'data_pub', 'autor', 'foi_publicada_recentemente')
    list_filter = ['data_pub']
    search_fields = ['texto']

admin.site.register(Pergunta, PerguntaAdmin)
# admin.site.register(Alternativa)